CLASS zcl_zgw_bupa_test_dpc_ext DEFINITION
  PUBLIC
  INHERITING FROM zcl_zgw_bupa_test_dpc
  CREATE PUBLIC .

  PUBLIC SECTION.
  PROTECTED SECTION.

    METHODS businesspartners_get_entityset
        REDEFINITION .
    METHODS businesspartners_get_entity
        REDEFINITION .
  PRIVATE SECTION.
ENDCLASS.

CLASS zcl_zgw_bupa_test_dpc_ext IMPLEMENTATION.

  METHOD businesspartners_get_entityset.
    DATA: lt_filters  TYPE                   /iwbep/t_mgw_select_option,
          ls_filter   TYPE                   /iwbep/s_mgw_select_option,
          ls_partner  TYPE /iwbep/s_cod_select_option,
          lr_partner  TYPE /iwbep/t_cod_select_options,
          ls_nameorg1 TYPE /iwbep/s_cod_select_option,
          lr_nameorg1 TYPE /iwbep/t_cod_select_options.

    READ TABLE it_filter_select_options INTO ls_filter WITH KEY property = 'PARTNER'.
    IF sy-subrc EQ 0.
      LOOP AT ls_filter-select_options INTO ls_partner.
        APPEND ls_partner TO lr_partner.
      ENDLOOP.
    ENDIF.

    READ TABLE it_filter_select_options INTO ls_filter WITH KEY property = 'NAME_ORG1'.
    IF sy-subrc EQ 0.
      LOOP AT ls_filter-select_options INTO ls_nameorg1.
        APPEND ls_nameorg1 TO lr_nameorg1.
      ENDLOOP.
    ENDIF.

    SELECT * FROM but000 INTO TABLE et_entityset WHERE partner IN lr_partner AND
                                                       name_org1 IN lr_nameorg1.

  ENDMETHOD.

  METHOD businesspartners_get_entity.
    READ TABLE it_key_tab INTO DATA(ls_key) INDEX 1.
    IF sy-subrc EQ 0.
      SELECT SINGLE * FROM but000 INTO CORRESPONDING FIELDS OF er_entity WHERE partner EQ ls_key-value.
    ENDIF.
  ENDMETHOD.

ENDCLASS.
