CLASS ltc_bupa_test_dpc_ext DEFINITION FOR TESTING
RISK LEVEL HARMLESS
DURATION SHORT.
  PRIVATE SECTION.
    DATA:
      " Request Structure filled to get the corresponding request context.
      ms_request_context_struct TYPE /iwbep/cl_mgw_request_unittst=>ty_s_mgw_request_context_unit,
      " Request context which is normally passed to each method call
      mo_request_context_object TYPE REF TO /iwbep/cl_mgw_request_unittst,
      " DPC referring to our extension call
      cut                       TYPE REF TO zcl_zgw_bupa_test_dpc_ext.
    METHODS setup.
    METHODS run_partner_single_filter FOR TESTING RAISING cx_static_check.
    METHODS run_partner_filter_sel_opt_eq FOR TESTING RAISING cx_static_check.
    METHODS run_partner_filter_sel_opt_bt FOR TESTING RAISING cx_static_check.
    METHODS run_partner_filter_sel_opt_le FOR TESTING RAISING cx_static_check.
    METHODS run_nameorg1_filter_sel_opt_cp FOR TESTING RAISING cx_static_check.

    METHODS set_context_struct.
    METHODS perform_entityset_ut
      IMPORTING
        it_filter_select_options TYPE /iwbep/t_mgw_select_option
      EXPORTING
        et_data                  TYPE REF TO data
        es_context               TYPE /iwbep/if_mgw_appl_srv_runtime=>ty_s_mgw_response_context.

ENDCLASS.

CLASS ltc_bupa_test_dpc_ext IMPLEMENTATION.
  "Created our data provider
  METHOD setup.
    CREATE OBJECT cut.
  ENDMETHOD.

  METHOD run_partner_filter_sel_opt_eq.
    DATA: lt_filter_select_options TYPE /iwbep/t_mgw_select_option,
          ls_filter_select_option  TYPE /iwbep/s_mgw_select_option,
          lt_filter                TYPE /iwbep/t_cod_select_options,
          ls_filter                TYPE /iwbep/s_cod_select_option,
          lr_data                  TYPE REF TO data,
          ls_context               TYPE /iwbep/if_mgw_appl_srv_runtime=>ty_s_mgw_response_context.
    FIELD-SYMBOLS: <tab> TYPE table.

    set_context_struct( ).

    " Fill the filter field detail
    ls_filter-sign = 'I'.
    ls_filter-option = 'EQ'.
    ls_filter-low = '0017100002'.
    APPEND ls_filter TO lt_filter.
    ls_filter_select_option-property = 'PARTNER'.
    ls_filter_select_option-select_options = lt_filter.
    APPEND ls_filter_select_option TO lt_filter_select_options.

    CALL METHOD perform_entityset_ut
      EXPORTING
        it_filter_select_options = lt_filter_select_options
      IMPORTING
        et_data                  = lr_data
        es_context               = ls_context.

    " If no data found for document type text then it is an exception
    ASSIGN lr_data->* TO <tab>.
    CALL METHOD cl_abap_unit_assert=>assert_not_initial
      EXPORTING
        act = <tab>. " Actual Data Object

  ENDMETHOD.

  METHOD run_partner_single_filter.
    DATA: lt_key_tab TYPE /iwbep/t_mgw_name_value_pair,
          ls_key_tab TYPE /iwbep/s_mgw_name_value_pair,
          lr_data    TYPE REF TO data,
          ls_context TYPE /iwbep/if_mgw_appl_srv_runtime=>ty_s_mgw_response_entity_cntxt.
    FIELD-SYMBOLS: <tab> TYPE any.

    set_context_struct( ).

    " Fill the filter field detail
    ls_key_tab-name = 'PARTNER'.
    ls_key_tab-value = '0017100002'.
    APPEND ls_key_tab TO lt_key_tab.

    " Every DPC class now has INIT_DP_FOR_UNIT_TEST method which is used to provide a data
    " instance which can be used for unit testing
    mo_request_context_object =  cut->/iwbep/if_mgw_conv_srv_runtime~init_dp_for_unit_test(
     is_request_context = ms_request_context_struct
     ).
    "read data
    TRY .
        cut->/iwbep/if_mgw_appl_srv_runtime~get_entity(
          EXPORTING
            io_tech_request_context   = mo_request_context_object
            it_key_tab                = lt_key_tab   " Table of key entries
          IMPORTING
            er_entity                 = lr_data
            es_response_context       = ls_context
        ).

      CATCH /iwbep/cx_mgw_busi_exception.

      CATCH /iwbep/cx_mgw_tech_exception.

    ENDTRY.
    " If no data found for document type text then it is an exception
    ASSIGN lr_data TO <tab>.
    CALL METHOD cl_abap_unit_assert=>assert_not_initial
      EXPORTING
        act = <tab>. " Actual Data Object

  ENDMETHOD.

  METHOD run_partner_filter_sel_opt_bt.
    DATA: lt_filter_select_options TYPE /iwbep/t_mgw_select_option,
          ls_filter_select_option  TYPE /iwbep/s_mgw_select_option,
          lt_filter                TYPE /iwbep/t_cod_select_options,
          ls_filter                TYPE /iwbep/s_cod_select_option,
          lr_data                  TYPE REF TO data,
          ls_context               TYPE /iwbep/if_mgw_appl_srv_runtime=>ty_s_mgw_response_context.
    FIELD-SYMBOLS: <tab> TYPE table.

    set_context_struct( ).

    " Fill the filter field detail
    ls_filter-sign = 'I'.
    ls_filter-option = 'BT'.
    ls_filter-low = '0017100002'.
    ls_filter-high = '0017100020'.
    APPEND ls_filter TO lt_filter.
    ls_filter_select_option-property = 'PARTNER'.
    ls_filter_select_option-select_options = lt_filter.
    APPEND ls_filter_select_option TO lt_filter_select_options.

    CALL METHOD perform_entityset_ut
      EXPORTING
        it_filter_select_options = lt_filter_select_options
      IMPORTING
        et_data                  = lr_data
        es_context               = ls_context.
    " If no data found for document type text then it is an exception
    ASSIGN lr_data->* TO <tab>.
    CALL METHOD cl_abap_unit_assert=>assert_not_initial
      EXPORTING
        act = <tab>. " Actual Data Object

  ENDMETHOD.

  METHOD run_partner_filter_sel_opt_le.
    DATA: lt_filter_select_options TYPE /iwbep/t_mgw_select_option,
          ls_filter_select_option  TYPE /iwbep/s_mgw_select_option,
          lt_filter                TYPE /iwbep/t_cod_select_options,
          ls_filter                TYPE /iwbep/s_cod_select_option,
          lr_data                  TYPE REF TO data,
          ls_context               TYPE /iwbep/if_mgw_appl_srv_runtime=>ty_s_mgw_response_context.
    FIELD-SYMBOLS: <tab> TYPE table.

    set_context_struct( ).

    " Fill the filter field detail
    ls_filter-sign = 'I'.
    ls_filter-option = 'LE'.
    ls_filter-low = '0017100020'.
    APPEND ls_filter TO lt_filter.
    ls_filter_select_option-property = 'PARTNER'.
    ls_filter_select_option-select_options = lt_filter.
    APPEND ls_filter_select_option TO lt_filter_select_options.

    " Every DPC class now has INIT_DP_FOR_UNIT_TEST method which is used to provide a data
    " instance which can be used for unit testing
    mo_request_context_object =  cut->/iwbep/if_mgw_conv_srv_runtime~init_dp_for_unit_test(
     is_request_context = ms_request_context_struct
     ).
    "read data
    TRY .
        cut->/iwbep/if_mgw_appl_srv_runtime~get_entityset(
          EXPORTING
            io_tech_request_context   = mo_request_context_object
            it_filter_select_options  = lt_filter_select_options    " Table of select options
          IMPORTING
            er_entityset              = lr_data
            es_response_context       = ls_context
        ).

      CATCH /iwbep/cx_mgw_busi_exception.

      CATCH /iwbep/cx_mgw_tech_exception.

    ENDTRY.
    " If no data found for document type text then it is an exception
    ASSIGN lr_data->* TO <tab>.
    CALL METHOD cl_abap_unit_assert=>assert_not_initial
      EXPORTING
        act = <tab>. " Actual Data Object
  ENDMETHOD.

  METHOD run_nameorg1_filter_sel_opt_cp.
    DATA: lt_filter_select_options TYPE /iwbep/t_mgw_select_option,
          ls_filter_select_option  TYPE /iwbep/s_mgw_select_option,
          lt_filter                TYPE /iwbep/t_cod_select_options,
          ls_filter                TYPE /iwbep/s_cod_select_option,
          lr_data                  TYPE REF TO data,
          ls_context               TYPE /iwbep/if_mgw_appl_srv_runtime=>ty_s_mgw_response_context.
    FIELD-SYMBOLS: <tab> TYPE table.

    set_context_struct( ).

    " Fill the filter field detail
    ls_filter-sign = 'I'.
    ls_filter-option = 'CP'.
    ls_filter-low = 'A*'.
    APPEND ls_filter TO lt_filter.
    ls_filter_select_option-property = 'NAME_ORG1'.
    ls_filter_select_option-select_options = lt_filter.
    APPEND ls_filter_select_option TO lt_filter_select_options.

    CALL METHOD perform_entityset_ut
      EXPORTING
        it_filter_select_options = lt_filter_select_options
      IMPORTING
        et_data                  = lr_data
        es_context               = ls_context.

    " If no data found for document type text then it is an exception
    ASSIGN lr_data->* TO <tab>.
    CALL METHOD cl_abap_unit_assert=>assert_not_initial
      EXPORTING
        act = <tab>. " Actual Data Object
  ENDMETHOD.

  METHOD set_context_struct.
    " Fill the data set and entity names to be unit tested
    ms_request_context_struct-technical_request-source_entity_type = 'BusinessPartner'.
    ms_request_context_struct-technical_request-target_entity_type = 'BusinessPartner'.
    ms_request_context_struct-technical_request-source_entity_set = 'BusinessPartnerSet'.
    ms_request_context_struct-technical_request-target_entity_set = 'BusinessPartnerSet'.
  ENDMETHOD.

  METHOD perform_entityset_ut.
    " Every DPC class now has INIT_DP_FOR_UNIT_TEST method which is used to provide a data
    " instance which can be used for unit testing
    mo_request_context_object =  cut->/iwbep/if_mgw_conv_srv_runtime~init_dp_for_unit_test(
     is_request_context = ms_request_context_struct
     ).

    "read data
    TRY .
        cut->/iwbep/if_mgw_appl_srv_runtime~get_entityset(
          EXPORTING
            io_tech_request_context   = mo_request_context_object
            it_filter_select_options  = it_filter_select_options    " Table of select options
          IMPORTING
            er_entityset              = et_data
            es_response_context       = es_context
        ).

      CATCH /iwbep/cx_mgw_busi_exception.

      CATCH /iwbep/cx_mgw_tech_exception.

    ENDTRY.
  ENDMETHOD.

ENDCLASS.
